package javadoc;

/**
 *
 * @author Pati
 */
 

public class Principal {
    
    /**
     * @param args the command line arguments
     */
/**
 * Si el metodo esCapikua falla, devolvera el error del metodo EsNegativoEX
 * @param args es un array de Strings que debe aparecer obligatoriamente como 
 * argumento del método main  
 * @see EsNegativoEX
 */
public static void main(String[] args) {
try {
System.out.println("12345 es capicúa: " + SoyUtil.esCapikua(12345));
System.out.println("1221 es capicúa: " + SoyUtil.esCapicua(1221));
System.out.println("1234321 es capicúa: " + SoyUtil.esCapikua(1234321));
} catch (EsNegativoEX ex) {
}
}
}