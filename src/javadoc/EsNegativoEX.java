package javadoc;

/**
 * Esta clase contiene los atributos y metodos del mensaje cuando 
 * el numero es negativo
 * @author Pati
 * @version %I%%G%
 * @see Exception
 */

/**
 * Código de la clase EsNegativoEX, subclase de la clase Exception
 */
public class EsNegativoEX extends Exception {
/**
 * mostrara el mensaje de error
 */
public EsNegativoEX() {
    super("el valor no puede ser negativo");
}
/**
 * metodo constructor del mensaje
 * @param msg mensaje que se mostrara en caso de error 
 */
public EsNegativoEX(String msg) {
super(msg);
}
}

