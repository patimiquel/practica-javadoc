package javadoc;

/**
 * Esta clase contiene los métodos que determinarán si los valores introducidos 
 * cumplen las condiciones para ser utiles-validos para el programa 
 * @author Pati
 * @version %I%%G%
 * @see EsNegativoEX
 */

public class SoyUtil {
 /**
 * Metodo que te verifica si el numero introducido es capicua
 * @param numero numero introducido
 * @return devuelve el valor true si el numero es capicua
 * @throws EsNegativoEX si el nº introducido es negativo 
 * @see EsNegativoEX
 * @deprecated As of release 1.35, replaced by {@link esCapikua(int numero)}
*/

@Deprecated 
public static boolean esCapicua(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX();
}
int numAlReves = 0;
int copia = numero;
while (numero > 0) {
numAlReves = numAlReves * 10 + numero % 10;
numero /= 10;
}
return copia == numAlReves;
}
/**
 * Metodo que te verifica si el numero introducido es capicua
 * @param numero numero introducido 
 * @return devuelve el valor true si el numero es capicua
 * @throws EsNegativoEX si el nº introducido es negativo 
 * @see EsNegativoEX
 */
public static boolean esCapikua(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX();
}
String cadNum = numero + "";
String numAlReves = (new StringBuilder(cadNum)).reverse().toString();
return cadNum.equals(numAlReves);
}
/**
 * Metodo que te verifica si el numero introducido es primo
 * @param numero numero introducido
 * @return devuelve el valor true si el numero es primo
 * @throws EsNegativoEX si el nº introducido es negativo 
 * @see EsNegativoEX
 */

public static boolean esPrimo(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX();
}
int limite = numero / 2 + 1;
int div = 2;
while (div < limite) {
if (numero % div == 0) {
return false;
}
div++;
}
return true;
}
/**
* Metodo que calcula el factorial del numero introducido
* @param numero numero introducido
*@throws EsNegativoEX si el nº introducido es negativo 
*@see <a href="https://www.sangakoo.com/es/temas/el-factorial-de-un-numero" target="_blank">Sangaku Maths</a>
*@see EsNegativoEX
* @return factorial del numero introducido
*/
public static long getFactorial(int numero) throws EsNegativoEX {
if (numero < 0) {
throw new EsNegativoEX(
"no se puede calcular el factorial de un número negativo");
}
long fact = 1L;
while (numero > 1) {
    fact *= numero;
numero--;
}
return fact;
}
}

    

